# Proxy


Scheme:

```mermaid
sequenceDiagram
	participant f as Fuzzer
	participant p as Proxy
	participant s as SOEM
	f ->> p: Fuzzdata

	loop until
	note right of s: until Fuzzer send new <br> or packets left
    s ->> p: some data
	p ->> s: send parsed packet
	p -->> f: check state "did it sent smth?"
	end
```

Packet strucrure:

```c
struct fuzz_packet[]{
    unsigned char size;
    unsigned char packet[size];
};
```



