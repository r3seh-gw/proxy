#include "fuzz_proxy.h"


int start_proxy(char *net_if, char *str_port) {
    char *stat;
    static struct timespec N_it;
    N_it.tv_sec = 0;
    N_it.tv_nsec = 100 * 1000000;
    int s_soem;
    char *recvbuf = malloc(0xfffff);
    int send_size = 0;
    int total_size = 0;
    int cur_off = 0;
    char *send_buf = NULL;
    int port = atoi(str_port);
    int r;
    if (port <= 1023) {
        printf("Specify userspace port (>1023)\n");
        return -1;
    }

    int soem_socket = init_if(net_if);
    if (soem_socket <= 0) {
        printf("Run as admin or specify correct interface");
        return -1;
    }

    int fuzz_socket = open_tcp_sock(port);

    char *buf = NULL;
    unsigned long int recv_size = 0;
    struct fuzz_packet* f_packet = NULL;
    int anyAction = 0;

    while (1) {
        if ((recv_size = get_fuzz_data(fuzz_socket, &buf)) > 0) {
            anyAction = 1;
            cur_off = 0;
            total_size = recv_size;
            f_packet = (struct fuzz_packet *)buf;
            printf("\nNew connect\n");
        }
        // TODO: If data comes to an int while a'm not ready to send packets.
        if (f_packet != NULL && cur_off < total_size) {
            anyAction = 1;
            printf("Try to get packets\r");
            if (recv(soem_socket, recvbuf, 0xffff, 0) <= 0) {
                // printf("Smth goes wrong on receive\n");
                // printf("recieve exit with error %s\n", strerror(errno));
                continue;
            }
            send_size = (cur_off + 1) + f_packet->size > total_size
                            ? total_size - (cur_off + 1)
                            : f_packet->size;
            send_buf = append_eth_header(f_packet->packet, send_size);
            printf("cur offset: %d\n", cur_off);
            r = send(soem_socket, send_buf, send_size + 0xe, 0);
            free(send_buf);
            if (r < 0) {
                printf("Smth goes wrong on send\n");
                continue;
            }
            cur_off += f_packet->size + 1;
            f_packet = (struct fuzz_packet *) buf + cur_off;
        } else {
            while (recv(soem_socket, recvbuf, 0xffff, 0) > 0);
        }
        if (!anyAction) {
            printf("Wait for connection\r");
            nanosleep(N_it, 0, 0);
        }
        anyAction = 0;
    }

    free(recvbuf);
    return 0;
}

unsigned long int get_fuzz_data(int ls, char **buf) {
    char *recvbuf = malloc(512);
    char *res = NULL;
    int readlen = 0;
    int client_s;
    socklen_t saddr_len = sizeof (struct sockaddr_storage);
    struct sockaddr_in saddr;
    unsigned long int total_read = 0;

    if ((client_s = accept4(ls, (struct sockaddr *)&saddr,
            &saddr_len, SOCK_CLOEXEC)) <= 0) {
        if ((errno != EAGAIN) && (errno != EWOULDBLOCK)) {
            printf("recieve exit with error %s\n", strerror(errno));
            return 0;
        }
        free(recvbuf);
        return 0;
    }

    do {
        if ((readlen = recv(client_s, recvbuf, 512, MSG_WAITALL)) > 0) {
            res = append(res, total_read, recvbuf, readlen);
            total_read += readlen;
        }
    } while (readlen > 0);

    free(recvbuf);
    if (total_read) {
        if (buf != NULL) {
            free(* buf);
        }
        (* buf) = res;
    }
    return total_read;
}

char *append(char *dst, unsigned long int dst_size, char *source, int s_size) {
    char *res = malloc(dst_size + s_size);
    memcpy(res, dst, dst_size);
    memcpy(res + dst_size, source, s_size);
    free(dst);
    return res;
}

int is_process_running(char *cmd) {
    DIR* dir;
    struct dirent* ent;
    char* endptr;
    char buf[512];
    int size;

    if (!(dir = opendir("/proc"))) {
        perror("can't open /proc");
        return 0;
    }

    while((ent = readdir(dir)) != NULL) {
        long lpid = strtol(ent->d_name, &endptr, 10);
        if (*endptr != '\0') {
            continue;
        }

        snprintf(buf, sizeof(buf), "/proc/%ld/cmdline", lpid);
        int fp = open(buf, O_RDONLY);

        if (fp) {
            if ((size = read(fp, buf, sizeof(buf))) > 0) {
                union_strs(buf, size);
                if (!strcmp(buf, cmd)) {
                    close(fp);
                    closedir(dir);
                    return 1;
                }
            }
            close(fp);
        }

    }

    closedir(dir);
    return 0;
}

void union_strs(char *buf, int size) {
    for(int i = 0; i < size - 1; i++) {
        if (buf[i] == '\0') {
            buf[i] = ' ';
        }
    }
}

int open_tcp_sock(int port) {
    int s;
    struct sockaddr_in sin;
    s = socket(AF_INET, SOCK_STREAM, 0);
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = htonl(INADDR_ANY);
    sin.sin_port = htons(port);
    fcntl(s, F_SETFL, fcntl(s, F_GETFL, 0) | O_NONBLOCK);
    bind(s, (struct sockaddr*)&sin, sizeof(sin));
    listen(s, 8);
    return s;
}

int init_if(char *ifname) 
{
    int i;
    int r, rval, ifindex;
    struct ifreq ifr;
    struct sockaddr_ll sll;
    int psock;
    rval = 0;

    psock = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ECAT));
    i = 1;
    r = setsockopt(psock, SOL_SOCKET, SO_DONTROUTE, &i, sizeof(i));
    strcpy(ifr.ifr_name, ifname);
    r = ioctl(psock, SIOCGIFINDEX, &ifr);
    ifindex = ifr.ifr_ifindex;
    strcpy(ifr.ifr_name, ifname);
    ifr.ifr_flags = 0;
    r = ioctl(psock, SIOCGIFFLAGS, &ifr);

    r = fcntl(psock, F_SETFL, fcntl(psock, F_GETFL) | O_NONBLOCK);
    ifr.ifr_flags = ifr.ifr_flags | IFF_PROMISC | IFF_BROADCAST;
    r = ioctl(psock, SIOCSIFFLAGS, &ifr);

    sll.sll_family = AF_PACKET;
    sll.sll_ifindex = ifindex;
    sll.sll_protocol = htons(ETH_P_ECAT);
    r = bind(psock, (struct sockaddr *)&sll, sizeof(sll));
    return psock;
}

char *append_eth_header(char *buf, size_t size) {
    char *a = malloc(size + 0xe);
    a[0] = 0xff;
    a[1] = 0xff;
    a[2] = 0xff;
    a[3] = 0xff;
    a[4] = 0xff;
    a[5] = 0xff;
    a[6] = 0x02;
    a[7] = 0x02;
    a[8] = 0x02;
    a[9] = 0x02;
    a[10] = 0x02;
    a[11] = 0x02;
    a[12] = 0x88;
    a[13] = 0xa4;
    memcpy(a + 0xe, buf, size);
    return a;
}

char *set_eth_header(char *buf) {
    (*(buf +  0)) = 0xff;
    (*(buf +  1)) = 0xff;
    (*(buf +  2)) = 0xff;
    (*(buf +  3)) = 0xff;
    (*(buf +  4)) = 0xff;
    (*(buf +  5)) = 0xff;
    (*(buf +  6)) = 0x02;
    (*(buf +  7)) = 0x02;
    (*(buf +  8)) = 0x02;
    (*(buf +  9)) = 0x02;
    (*(buf + 10)) = 0x02;
    (*(buf + 11)) = 0x02;
    (*(buf + 12)) = 0x88;
    (*(buf + 13)) = 0xa4;
    return buf;
}

int main(int argc, char *argv[]) {
    char *net_if = NULL;
    char *port_num = NULL;
    printf("argc: %d\n", argc);
    if (argc <= 2) {
        printf("You need to specify interface and port\n"
               "Interface for ethercat connection\n"
               "And port for interaction with Fuzzer\n"
               "./proxy <int_name> <port_num>\n"
                );
        return 1;
    } else if (argc == 3) {
        net_if = argv[1];
        port_num = argv[2];
    } else {
        printf("What's the fuck");
    }

    if (start_proxy(net_if, port_num)) {
        printf("ALARM!!!!!\n");
    } else {
        printf("Keep calm!\nEvrth is fine ;-)\n");
    }

    return 0;
}
