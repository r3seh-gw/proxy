#include <sys/ioctl.h>
#include <net/if.h>
#include <arpa/inet.h> // htons
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netpacket/packet.h>
#include <errno.h>
#include <unistd.h> // read
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>

#define u8 unsigned char
#define u32 unsigned int
#define ETH_P_ECAT 0x88a4

struct fuzz_packet{
    unsigned char size;
    unsigned char packet[];
};


int init_if(char *ifname);

int is_process_running(char *cmd);

int start_proxy(char *filename, char *port);

char *append_eth_header(char *buf, size_t size);

char *set_eth_header(char *buf);

int open_tcp_sock(int port);

unsigned long int get_fuzz_data(int ls, char **buf);

// HELP FUNCS

void union_strs(char *buf, int size);

char *append(char *dst, unsigned long int dst_size, char *source, int s_size);
